package fr.eql.ai113.lmtravelsFront.managed.bean;

import fr.eql.ai113.lmtravelsFront.controller.webclient.PacksWebClient;
import fr.eql.ai113.lmtravelsFront.entity.Accommodation;
import fr.eql.ai113.lmtravelsFront.entity.Booking;
import fr.eql.ai113.lmtravelsFront.entity.Pack;
import fr.eql.ai113.lmtravelsFront.entity.User;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthResponse;
import fr.eql.ai113.lmtravelsFront.entity.dto.BookingDto;
import fr.eql.ai113.lmtravelsFront.util.AuthenticationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;


@Component(value = "mbPacks")
@Scope(value = "request")
public class PacksManagedBean {

    private List<Pack> packList;
    private Integer id;


    /// For Packs Bookings ///
    private User user;
    private Pack aPack;
    private User connectedUser;

    /// For Accommodation Booking ///
    private Accommodation accommodation;
    private Date booking_date;
    public Date getBooking_date() {
        return booking_date;
    }
    public void setBooking_date(Date booking_date) {
        this.booking_date = booking_date;
    }

    private Booking booking;
    public PacksManagedBean() {
        this.booking = new Booking();
    }


    private PacksWebClient packsWebClient;
    @Autowired
    public void setPacksWebClient(PacksWebClient packsWebClient) {
        this.packsWebClient = packsWebClient;
    }

    /**
     * This method allows to retrieve all Packs.
     * @return all Packs.
     */
    public List<Pack> findAllPacks() {
        return packsWebClient.findAllPacks();
    }

    /**
     * This method allows to retrieve all Bookings.
     * @return all Bookings.
     */
    public List<Booking> findAllBookings() {
        return packsWebClient.findAllBookings();
    }

    /**
     * This method allows to retrieve all Bookings from a User
     * @param userid
     * @return List of User's bookings
     */
    public List<Booking> getUserBookings(Integer userid) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        AuthResponse responseDto = (AuthResponse) session.getAttribute("connectedUserWithToken");
        User connectedUser = responseDto.getUser();

        return packsWebClient.getUserBookings(connectedUser.getId());
    }

    /**
     * This method allows to retrieve all Accommodations.
     * @return all Accommodations.
     */
    public List<Accommodation> findAllAccommodations() {
        return packsWebClient.findAllAccommodations();
    }


    /**
     * This method allows to Book a Pack
     * @param savedPack
     */
    public void saveBooking(Pack savedPack) {
        User connectedUser = AuthenticationUtils.getConnectedUserWithToken();
        if (connectedUser != null) {
            BookingDto newBooking = new BookingDto(
                    connectedUser,
                    savedPack
            );
            packsWebClient.saveBooking(newBooking);

            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_INFO,
                    "Formule réservée correctement !",
                    "Formule réservée correctement !"
            );
            FacesContext.getCurrentInstance().addMessage("saveBooking:inpSaveBooking", facesMessage);
        } else {
            // Handle case when connected user is not available
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "Vous devez être connecté pour effectuer une réservation.",
                    "Vous devez être connecté pour effectuer une réservation."
            );
            FacesContext.getCurrentInstance().addMessage("saveBooking:inpSaveBooking", facesMessage);
        }
    }

    /**
     * This method Allows a User to Book an Accommodation
     * @param savedAccommodation
     * @param booking_date
     */
    public void accommodationBooking(Accommodation savedAccommodation, Date booking_date) {
        System.out.println("booking_date: " + booking_date);
//        booking_date = new Date();
        System.out.println("booking_date parameter: " + booking_date);
        User connectedUser = AuthenticationUtils.getConnectedUserWithToken();
        if (connectedUser != null) {
            BookingDto newBooking = new BookingDto(
                    booking_date,
                    connectedUser,
                    savedAccommodation
            );

            packsWebClient.accommodationBooking(newBooking);

            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_INFO,
                    "Hébergement réservé correctement !",
                    "Hébergement réservé correctement !"
            );
            FacesContext.getCurrentInstance().addMessage("saveAccommodation:inpSaveAccommodation", facesMessage);
        } else {
            // Handle case when connected user is not available
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "Vous devez être connecté pour effectuer une réservation.",
                    "Vous devez être connecté pour effectuer une réservation."
            );
            FacesContext.getCurrentInstance().addMessage("saveAccommodation:inpSaveAccommodation", facesMessage);
        }
    }

    /**
     * This method allows to erase a Booking
     */
    public void cancelBooking() {packsWebClient.cancelBooking(id);}



    /// For PACKS///
    /// Getters ///
    public List<Pack> getPackList() {
        return packList;
    }
    public Integer getId() {
        return id;
    }
    ///Setters ///
    public void setPackList(List<Pack> packList) {
        this.packList = packList;
    }
    public void setId(Integer id) {
        this.id = id;
    }


    /// For BOOKING ///
    /// Getters ///
    public User getUser() {
        return user;
    }
    public Pack getaPack() {
        return aPack;
    }
    public PacksWebClient getPacksWebClient() {
        return packsWebClient;
    }
    public User getConnectedUser() {
        return connectedUser;
    }
    public Accommodation getAccommodation() {
        return accommodation;
    }
    public Booking getBooking() {
        return booking;
    }
    /// Setters///
    public void setUser(User user) {
        this.user = user;
    }
    public void setaPack(Pack aPack) {
        this.aPack = aPack;
    }
    public void setConnectedUser(User connectedUser) {
        this.connectedUser = connectedUser;
    }
    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }
    public void setBooking(Booking booking) {
        this.booking = booking;
    }

}
