package fr.eql.ai113.lmtravelsFront.managed.bean;

import fr.eql.ai113.lmtravelsFront.controller.webclient.LoginWebClient;
import fr.eql.ai113.lmtravelsFront.controller.webclient.RegistrationWebClient;
import fr.eql.ai113.lmtravelsFront.entity.Accommodation;
import fr.eql.ai113.lmtravelsFront.entity.User;
import fr.eql.ai113.lmtravelsFront.entity.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component(value = "mbRegistration")
@Scope(value = "request")
public class RegistrationManagedBean {

    private Integer id;
    private String newUserName;
    private String newUserSurname;
    private String newUserLogin;
    private String newUserPassword;
    private String newUserPhone;
    private String newUserAddress;


    private RegistrationWebClient registrationWebClient;

    @Autowired
    public void setRegistrationWebClient(RegistrationWebClient registrationWebClient) {
        this.registrationWebClient = registrationWebClient;}


    /**
     * This method register a new User.
     */

    public void registrationUser() {
        // Validate input data
        if (newUserName == null || newUserName.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Prenom obligatoire",
                    "Veuillez saisir votre Prenom"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserName", errorMessage);
            return;
        }

        if (newUserSurname == null || newUserSurname.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Nom obligatoire",
                    "Veuillez saisir votre Nom"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserSurname", errorMessage);
            return;
        }

        if (newUserLogin == null || newUserLogin.length() <= 8 || !Character.isUpperCase(newUserLogin.charAt(0))) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Nom utilisateur invalide",
                    "Le Nom utilisateur doit contenir plus de 8 caractères et commencer par une lettre majuscule"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserLogin", errorMessage);
            return;
        }

        if (newUserPassword == null || newUserPassword.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Mot de passe obligatoire",
                    "Veuillez saisir votre mot de passe"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserPassword", errorMessage);
            return;
        }

        if (newUserPhone == null || !newUserPhone.matches("\\d{10}")) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Numéro de téléphone invalide",
                    "Le numéro de téléphone doit contenir 10 chiffres"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserPhone", errorMessage);
            return;
        }

        if (newUserAddress == null || !newUserAddress.matches("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Z|a-z]{2,}\\b")) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Adresse e-mail invalide",
                    "L'adresse e-mail doit être au format exemple@domaine.com"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserAddress", errorMessage);
            return;
        }

        // Input data is valid, proceed with registration
        UserDto newUser = new UserDto(
                newUserName,
                newUserSurname,
                newUserLogin,
                newUserPassword,
                newUserPhone,
                newUserAddress
        );
        registrationWebClient.registration(newUser);

        FacesMessage successMessage = new FacesMessage(
                FacesMessage.SEVERITY_INFO,
                "Nouveau utilisateur enregistré correctement !",
                "Nouveau utilisateur enregistré correctement !"
        );
        FacesContext.getCurrentInstance().addMessage("registration:inpRegistration", successMessage);
    }


    /**
     * This method register a new User with Role permission Administrator.
     */
    public void registrationAdmin() {
        // Validate input data
        if (newUserName == null || newUserName.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Prenom obligatoire",
                    "Veuillez saisir votre Prenom"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserName", errorMessage);
            return;
        }

        if (newUserSurname == null || newUserSurname.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Nom obligatoire",
                    "Veuillez saisir votre Nom"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserSurname", errorMessage);
            return;
        }

        if (newUserLogin == null || newUserLogin.length() <= 8 || !Character.isUpperCase(newUserLogin.charAt(0))) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Nom utilisateur invalide",
                    "Le Nom utilisateur doit contenir plus de 8 caractères et commencer par une lettre majuscule"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserLogin", errorMessage);
            return;
        }

        if (newUserPassword == null || newUserPassword.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Mot de passe obligatoire",
                    "Veuillez saisir votre mot de passe"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserPassword", errorMessage);
            return;
        }

        if (newUserPhone == null || !newUserPhone.matches("\\d{10}")) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Numéro de téléphone invalide",
                    "Le numéro de téléphone doit contenir 10 chiffres"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserPhone", errorMessage);
            return;
        }

        if (newUserAddress == null || !newUserAddress.matches("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Z|a-z]{2,}\\b")) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Adresse e-mail invalide",
                    "L'adresse e-mail doit être au format exemple@domaine.com"
            );
            FacesContext.getCurrentInstance().addMessage("registration:inpNewUserAddress", errorMessage);
            return;
        }

        // Input data is valid, proceed with registration
        UserDto newUser = new UserDto(
                newUserName,
                newUserSurname,
                newUserLogin,
                newUserPassword,
                newUserPhone,
                newUserAddress
        );
        registrationWebClient.registrationAdmin(newUser);

        FacesMessage successMessage = new FacesMessage(
                FacesMessage.SEVERITY_INFO,
                "Nouveau Administrateur enregistré correctement !",
                "Nouveau Administrateur enregistré correctement !"
        );
        FacesContext.getCurrentInstance().addMessage("registration:inpRegistration", successMessage);
    }

    /**
     * This method allows to retrieve all Users.
     * @return all User.
     */
    public List<User> findAllUsers() {
        return registrationWebClient.findAllUsers();
    }

    /**
     * This method allows to delete a User
     */
    public void deleteUser() {registrationWebClient.deleteUser(id);}




    /// Getters ///
    public Integer getId() {
        return id;
    }
    public String getNewUserName() {
        return newUserName;
    }
    public String getNewUserSurname() {
        return newUserSurname;
    }
    public String getNewUserLogin() {
        return newUserLogin;
    }
    public String getNewUserPassword() {
        return newUserPassword;
    }
    public String getNewUserPhone() {
        return newUserPhone;
    }
    public String getNewUserAddress() {
        return newUserAddress;
    }

    /// Setters ///
    public void setId(Integer id) {
        this.id = id;
    }
    public void setNewUserName(String newUserName) {
        this.newUserName = newUserName;
    }
    public void setNewUserSurname(String newUserSurname) {
        this.newUserSurname = newUserSurname;
    }
    public void setNewUserLogin(String newUserLogin) {
        this.newUserLogin = newUserLogin;
    }
    public void setNewUserPassword(String newUserPassword) {
        this.newUserPassword = newUserPassword;
    }
    public void setNewUserPhone(String newUserPhone) {
        this.newUserPhone = newUserPhone;
    }
    public void setNewUserAddress(String newUserAddress) {
        this.newUserAddress = newUserAddress;
    }

}
