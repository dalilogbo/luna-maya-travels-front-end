package fr.eql.ai113.lmtravelsFront.managed.bean;

import fr.eql.ai113.lmtravelsFront.controller.webclient.LoginWebClient;
import fr.eql.ai113.lmtravelsFront.entity.Role;
import fr.eql.ai113.lmtravelsFront.entity.User;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthRequest;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.Collection;

@Component(value = "mbLogin")
@Scope(value = "session")
public class LoginManagedBean {

    private String login;
    private String password;
    private User connectedUser;

    /** Injected by the setter */
    private LoginWebClient loginWebClient;
    @Autowired
    public void setLoginWebClient(LoginWebClient loginWebClient) {
        this.loginWebClient = loginWebClient;
    }



    public String connect() {
        String forward = "/connection?faces-redirect=false";
        AuthRequest requestDto = new AuthRequest(login, password);
        AuthResponse connectedUserWithToken = loginWebClient.authenticate(requestDto);
        if (connectedUserWithToken != null) {
            connectedUser = connectedUserWithToken.getUser();
            Collection<Role> roles = connectedUser.getRoles();
            if (roles.stream().anyMatch(role -> role.getName().equals("ADMIN"))) {
                forward = "/homeAdmin?faces-redirect=true";
            } else if (roles.stream().anyMatch(role -> role.getName().equals("CLIENT"))) {
                forward = "/home?faces-redirect=true";
            } else {
                FacesMessage facesMessage = new FacesMessage(
                        FacesMessage.SEVERITY_WARN,
                        "Identifiant et/ou mot de passe incorrect(s)",
                        "Identifiant et/ou mot de passe incorrect(s)"
                );
                FacesContext.getCurrentInstance().addMessage("loginForm:inpLogin", facesMessage);
                FacesContext.getCurrentInstance().addMessage("loginForm:inpPassword", facesMessage);
            }
        } else {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_WARN,
                    "Identifiant et/ou mot de passe incorrect(s)",
                    "Identifiant et/ou mot de passe incorrect(s)"
            );
            FacesContext.getCurrentInstance().addMessage("loginForm:inpLogin", facesMessage);
            FacesContext.getCurrentInstance().addMessage("loginForm:inpPassword", facesMessage);
        }
        return forward;
    }



    public boolean isConnected() {return connectedUser != null;}

    public void authorise() {
        FacesContext context = FacesContext.getCurrentInstance();
        ConfigurableNavigationHandler handler = (ConfigurableNavigationHandler)
                context.getApplication().getNavigationHandler();
        if (!isConnected()) {
            handler.performNavigation("/connection.xhtml?faces-redirect=true");
        }
    }

    public String disconnect() {
        HttpSession session = (HttpSession) FacesContext
                .getCurrentInstance()
                .getExternalContext()
                .getSession(true);
        session.invalidate();
        login = "";
        password = "";
        connectedUser = null;
        return  "/connection.xhtml?faces-redirect=true";
    }

    /// Getters ///
    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
    public User getConnectedUser() {
        return connectedUser;
    }


    /// Setters ///
    public void setLogin(String login) {
        this.login = login;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setConnectedUser(User connectedUser) {
        this.connectedUser = connectedUser;
    }


}
