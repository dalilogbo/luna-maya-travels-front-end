package fr.eql.ai113.lmtravelsFront.managed.bean;

import fr.eql.ai113.lmtravelsFront.controller.webclient.NewPacksWebClient;
import fr.eql.ai113.lmtravelsFront.entity.Pack;
import fr.eql.ai113.lmtravelsFront.entity.dto.AccommodationDto;
import fr.eql.ai113.lmtravelsFront.entity.dto.PackDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Component(value = "mbPackCreation")
@Scope(value="request")
public class NewPacksManagedBean {

    /// New Pack ///
    private List<Pack> packList;
    private Integer id;
    private String new_pack_name;
    private String new_pack_description;
    private String new_pack_photo;
    private Boolean new_pack_disponibility;
    private Date new_pack_depart_date;
    private Date new_pack_return_date;
    private Integer new_pack_price;
    public String new_pack_accomodation;
    public String new_pack_transport;
    public String new_pack_activity;

    /// New Accommodation ///
    public  String new_accom_type;
    public  String new_accom_name;
    public  String new_accom_country;
    public  String new_accom_city;
    public  String new_accom_description;
    public  String new_accom_room_type;
    public  String new_accom_photo;
    public Boolean new_accom_disponibility;
    public Date new_accom_arrival_date;
    public Date new_accom_depart_date;
    public Integer new_accom_price;


    private NewPacksWebClient newPacksWebClient;
    @Autowired
    public void setNewPacksWebClient(NewPacksWebClient newPacksWebClient) {
        this.newPacksWebClient = newPacksWebClient;
    }

    /**
     * This Méthode allows the creation of a new Pack with validation contraints.
     */
    public void creationPack() {
        // Perform simple validation checks
        if (new_pack_name == null || new_pack_name.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Le nom du pack est requis !",
                    "Le nom du pack est requis !"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewPackName", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_description == null || new_pack_description.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La description du pack est requise !",
                    "La description du pack est requise !"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewPackDescription", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_photo == null || new_pack_photo.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La photo du pack est requise !",
                    "La photo du pack est requise !"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewPackPhoto", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_disponibility == null) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La disponibilité du pack est requise !",
                    "La disponibilité du pack est requise !"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewPackDisponibility", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_depart_date == null || new_pack_return_date == null) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La date de départ et la date de retour du pack sont requises !",
                    "La date de départ et la date de retour du pack sont requises !"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewDatesPack", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_price == null || new_pack_price <= 0) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Le prix du pack doit être supérieur à zéro !",
                    "Le prix du pack doit être supérieur à zéro !"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewPackPrice", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_accomodation == null || new_pack_accomodation.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "L'hébergement du pack est requis !",
                    "L'hébergement du pack est requis !"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewPackAccommodation", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_transport == null || new_pack_transport.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Le transport du pack est requis !",
                    "Le transport du pack est requis !"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewPackTransport", facesMessage);
            return; // Return early if validation fails
        }
        if (new_pack_activity == null || new_pack_activity.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La description des activités du pack sont requises !",
                    "La description des activités du pack sont requises"
            );
            FacesContext.getCurrentInstance().addMessage("creationPack:inpNewPackActivity", facesMessage);
            return; // Return early if validation fails
        }

        // Convert the input values to PackDto object
        PackDto newPack = new PackDto(
                new_pack_name,
                new_pack_description,
                new_pack_photo,
                new_pack_disponibility,
                new_pack_depart_date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                new_pack_return_date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                new_pack_price,
                new_pack_accomodation,
                new_pack_transport,
                new_pack_activity
        );
        newPacksWebClient.createPack(newPack);

        FacesMessage facesMessage = new FacesMessage(
                FacesMessage.SEVERITY_INFO,
                "Nouvelle formule enregistrée correctement !",
                "Nouvelle formule enregistrée correctement !"
        );
        FacesContext.getCurrentInstance().addMessage("creationPack:inpCreationPack", facesMessage);
    }


    /**
     * This method allows to retrieve all Packs.
     * @return all Packs.
     */
    public List<Pack> findAllPacks() {
        return newPacksWebClient.findAllPacks();
    }


    /**
     * This method allows to update a Pack's information.
     */
    public void updatePack() {
        // Perform simple validation checks
        if (id == null) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "ID d'hébergement obligatoire",
                    "Veuillez saisir ID d'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdateID", errorMessage);
            return; // Return early if validation fails
        }
        if (new_pack_name == null || new_pack_name.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Le nom de la formule est requis !",
                    "Le nom de la formule est requis !"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePackName", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_description == null || new_pack_description.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La description de la formule est requise !",
                    "La description de la formule est requise !"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePackDescription", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_photo == null || new_pack_photo.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La photo de la formule est requise !",
                    "La photo de la formule est requise !"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePackPhoto", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_disponibility == null) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La disponibilité de la formule est requise !",
                    "La disponibilité de la formule est requise !"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePackDisponibility", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_depart_date == null || new_pack_return_date == null) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La date de départ et la date de retour de la formule sont requises !",
                    "La date de départ et la date de retour de la formule sont requises !"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdateDatesPack", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_price == null || new_pack_price <= 0) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Le prix de la formule doit être supérieur à zéro !",
                    "Le prix de la formule être supérieur à zéro !"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePackPrice", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_accomodation == null || new_pack_accomodation.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "L'hébergement de la formule est requis !",
                    "L'hébergement de la formule est requis !"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePackAccommodation", facesMessage);
            return; // Return early if validation fails
        }

        if (new_pack_transport == null || new_pack_transport.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Le transport de la formule est requis !",
                    "Le transport de la formule est requis !"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePackTransport", facesMessage);
            return; // Return early if validation fails
        }
        if (new_pack_activity == null || new_pack_activity.isEmpty()) {
            FacesMessage facesMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "La description des activités de la formule sont requises !",
                    "La description des activités de la formule sont requises"
            );
            FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePackActivity", facesMessage);
            return; // Return early if validation fails
        }

        // Convert the input values to PackDto object
        PackDto updatePack = new PackDto(
                id,
                new_pack_name,
                new_pack_description,
                new_pack_photo,
                new_pack_disponibility,
                new_pack_depart_date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                new_pack_return_date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                new_pack_price,
                new_pack_accomodation,
                new_pack_transport,
                new_pack_activity
        );
        newPacksWebClient.updatePack(updatePack);

        FacesMessage facesMessage = new FacesMessage(
                FacesMessage.SEVERITY_INFO,
                "Nouvelle formule modifiée correctement !",
                "Nouvelle formule modifiée correctement !"
        );
        FacesContext.getCurrentInstance().addMessage("UpdatePack:inpUpdatePack", facesMessage);
    }

    /**
     * This method allows to erase a Pack.
     */
    public void deletePack() {
        newPacksWebClient.deletePack(id);
    }


    /**
     * This Méthode allows the creation of new Accommodation with validation contraints.
     */
    public void createAccommodation() {
        // Validate input data
        if (new_accom_type == null || new_accom_type.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Type d'hébergement obligatoire",
                    "Veuillez choisir un type d'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomType", errorMessage);
            return;
        }

        if (new_accom_name == null || new_accom_name.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Nom d'hébergement obligatoire",
                    "Veuillez saisir le nom de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomName", errorMessage);
            return;
        }

        if (new_accom_country == null || new_accom_country.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Pays d'hébergement obligatoire",
                    "Veuillez saisir le pays de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomCountry", errorMessage);
            return;
        }

        if (new_accom_city == null || new_accom_city.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Ville d'hébergement obligatoire",
                    "Veuillez saisir la ville de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomCity", errorMessage);
            return;
        }

        if (new_accom_description == null || new_accom_description.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Description d'hébergement obligatoire",
                    "Veuillez saisir la description de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomDescription", errorMessage);
            return;
        }

        if (new_accom_room_type == null || new_accom_room_type.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Type de chambre obligatoire",
                    "Veuillez choisir un type de chambre"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomRoomType", errorMessage);
            return;
        }

        if (new_accom_photo == null || new_accom_photo.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Photo d'hébergement obligatoire",
                    "Veuillez saisir le nom de la photo de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomPhoto", errorMessage);
            return;
        }

        if (new_accom_disponibility == null) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Disponibilité d'hébergement obligatoire",
                    "Veuillez saisir la disponibilité de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomDisponibility", errorMessage);
            return;
        }

        if (new_accom_price == null) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Prix d'hébergement obligatoire",
                    "Veuillez saisir le prix de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpNewAccomPrice", errorMessage);
            return;
        }
        // Input data is valid, proceed with registration of new Accommodation
        AccommodationDto newAccommodation = new AccommodationDto(
                new_accom_type,
                new_accom_name,
                new_accom_country,
                new_accom_city,
                new_accom_description,
                new_accom_room_type,
                new_accom_photo,
                new_accom_disponibility,
                new_accom_price
        );
        newPacksWebClient.createAccommodation(newAccommodation);

        FacesMessage facesMessage = new FacesMessage(
                FacesMessage.SEVERITY_INFO,
                "Nouveau hébergement enregistré correctement !",
                "Nouveau hébergement enregistré correctement !"
        );
        FacesContext.getCurrentInstance().addMessage("creationAccommodation:inpCreationAccommodation", facesMessage);
    }



    /**
     * This method allows to erase Accommodation.
     */
    public void deleteAccommodation() {
        newPacksWebClient.deleteAccommodation(id);
    }


    /**
     * This method allows to update a Pack's information with validation contraints.
     */
    public void updateAccommodation() {
        // Validate input data
        if (id == null ) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "ID d'hébergement obligatoire",
                    "Veuillez saisir ID d'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateID", errorMessage);
            return;
        }
        if (new_accom_type == null || new_accom_type.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Type d'hébergement obligatoire",
                    "Veuillez choisir un type d'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomType", errorMessage);
            return;
        }

        if (new_accom_name == null || new_accom_name.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Nom d'hébergement obligatoire",
                    "Veuillez saisir le nom de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomName", errorMessage);
            return;
        }

        if (new_accom_country == null || new_accom_country.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Pays d'hébergement obligatoire",
                    "Veuillez saisir le pays de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomCountry", errorMessage);
            return;
        }

        if (new_accom_city == null || new_accom_city.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Ville d'hébergement obligatoire",
                    "Veuillez saisir la ville de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomCity", errorMessage);
            return;
        }

        if (new_accom_description == null || new_accom_description.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Description d'hébergement obligatoire",
                    "Veuillez saisir la description de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomDescription", errorMessage);
            return;
        }

        if (new_accom_room_type == null || new_accom_room_type.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Type de chambre obligatoire",
                    "Veuillez choisir un type de chambre"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomRoomType", errorMessage);
            return;
        }

        if (new_accom_photo == null || new_accom_photo.isEmpty()) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Photo d'hébergement obligatoire",
                    "Veuillez saisir le nom de la photo de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomPhoto", errorMessage);
            return;
        }

        if (new_accom_disponibility == null) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Disponibilité d'hébergement obligatoire",
                    "Veuillez saisir la disponibilité de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomDisponibility", errorMessage);
            return;
        }

        if (new_accom_price == null) {
            FacesMessage errorMessage = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Prix d'hébergement obligatoire",
                    "Veuillez saisir le prix de l'hébergement"
            );
            FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccomPrice", errorMessage);
            return;
        }
        // Input data is valid, proceed with registration of new Accommodation
        AccommodationDto updateAccommodation = new AccommodationDto(
                id,
                new_accom_type,
                new_accom_name,
                new_accom_country,
                new_accom_city,
                new_accom_description,
                new_accom_room_type,
                new_accom_photo,
                new_accom_disponibility,
                new_accom_price
        );
        newPacksWebClient.updateAccommodation(updateAccommodation);

        FacesMessage facesMessage = new FacesMessage(
                FacesMessage.SEVERITY_INFO,
                "Mise a jour enregistré correctement !",
                "Mise a jour enregistré correctement !"
        );
        FacesContext.getCurrentInstance().addMessage("updateAccommodation:inpUpdateAccommodation", facesMessage);
    }


    /// Getters New Pack///
    public Integer getId() {
        return id;
    }
    public String getNew_pack_name() {
        return new_pack_name;
    }
    public String getNew_pack_description() {
        return new_pack_description;
    }
    public String getNew_pack_photo() {
        return new_pack_photo;
    }
    public Boolean getNew_pack_disponibility() {
        return new_pack_disponibility;
    }
    public Date getNew_pack_depart_date() {
        return new_pack_depart_date;
    }
    public Date getNew_pack_return_date() {
        return new_pack_return_date;
    }
    public Integer getNew_pack_price() {
        return new_pack_price;
    }
    public String getNew_pack_accomodation() {
        return new_pack_accomodation;
    }
    public String getNew_pack_transport() {
        return new_pack_transport;
    }
    public String getNew_pack_activity() {
        return new_pack_activity;
    }

    public List<Pack> getPackList() {
        return packList;
    }


    /// Setters New Pack///
    public void setId(Integer id) {
        this.id = id;
    }
    public void setNew_pack_name(String new_pack_name) {
        this.new_pack_name = new_pack_name;
    }
    public void setNew_pack_description(String new_pack_description) {
        this.new_pack_description = new_pack_description;
    }
    public void setNew_pack_photo(String new_pack_photo) {
        this.new_pack_photo = new_pack_photo;
    }
    public void setNew_pack_disponibility(Boolean new_pack_disponibility) {
        this.new_pack_disponibility = new_pack_disponibility;
    }
    public void setNew_pack_depart_date(Date new_pack_depart_date) {
        this.new_pack_depart_date = new_pack_depart_date;
    }
    public void setNew_pack_return_date(Date new_pack_return_date) {
        this.new_pack_return_date = new_pack_return_date;
    }
    public void setNew_pack_price(Integer new_pack_price) {
        this.new_pack_price = new_pack_price;
    }
    public void setNew_pack_accomodation(String new_pack_accomodation) {
        this.new_pack_accomodation = new_pack_accomodation;
    }
    public void setNew_pack_transport(String new_pack_transport) {
        this.new_pack_transport = new_pack_transport;
    }
    public void setNew_pack_activity(String new_pack_activity) {
        this.new_pack_activity = new_pack_activity;
    }

    public void setPackList(List<Pack> packList) {
        this.packList = packList;
    }


    /// Getters New Accommodation ///
    public String getNew_accom_type() {
        return new_accom_type;
    }
    public String getNew_accom_name() {
        return new_accom_name;
    }
    public String getNew_accom_country() {
        return new_accom_country;
    }
    public String getNew_accom_city() {
        return new_accom_city;
    }
    public String getNew_accom_description() {
        return new_accom_description;
    }
    public String getNew_accom_room_type() {
        return new_accom_room_type;
    }
    public String getNew_accom_photo() {
        return new_accom_photo;
    }
    public Boolean getNew_accom_disponibility() {
        return new_accom_disponibility;
    }
    public Date getNew_accom_arrival_date() {
        return new_accom_arrival_date;
    }
    public Date getNew_accom_depart_date() {
        return new_accom_depart_date;
    }
    public Integer getNew_accom_price() {
        return new_accom_price;
    }


    /// Setters New Accommodation ///
    public void setNew_accom_type(String new_accom_type) {
        this.new_accom_type = new_accom_type;
    }
    public void setNew_accom_name(String new_accom_name) {
        this.new_accom_name = new_accom_name;
    }
    public void setNew_accom_country(String new_accom_country) {
        this.new_accom_country = new_accom_country;
    }
    public void setNew_accom_city(String new_accom_city) {
        this.new_accom_city = new_accom_city;
    }
    public void setNew_accom_description(String new_accom_description) {
        this.new_accom_description = new_accom_description;
    }
    public void setNew_accom_room_type(String new_accom_room_type) {
        this.new_accom_room_type = new_accom_room_type;
    }
    public void setNew_accom_photo(String new_accom_photo) {
        this.new_accom_photo = new_accom_photo;
    }
    public void setNew_accom_disponibility(Boolean new_accom_disponibility) {
        this.new_accom_disponibility = new_accom_disponibility;
    }
    public void setNew_accom_arrival_date(Date new_accom_arrival_date) {
        this.new_accom_arrival_date = new_accom_arrival_date;
    }
    public void setNew_accom_depart_date(Date new_accom_depart_date) {
        this.new_accom_depart_date = new_accom_depart_date;
    }
    public void setNew_accom_price(Integer new_accom_price) {
        this.new_accom_price = new_accom_price;
    }


}
