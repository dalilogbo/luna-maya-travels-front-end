package fr.eql.ai113.lmtravelsFront.controller.webclient;

import fr.eql.ai113.lmtravelsFront.entity.Accommodation;
import fr.eql.ai113.lmtravelsFront.entity.Booking;
import fr.eql.ai113.lmtravelsFront.entity.Pack;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthResponse;
import fr.eql.ai113.lmtravelsFront.entity.dto.BookingDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.WebClient;


import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class PacksWebClient {

    private final WebClient webClient;


    public PacksWebClient() {
        this.webClient = WebClient
                .builder()
                .filter(ErrorHandler.handle())
                .baseUrl(BackUrl.BACK_URL + "packs/")
                .build();
    }


    public List<Pack> findAllPacks() {
        List<Pack> packs = webClient.get()
                .uri("findAllPacks")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Pack>>() {})
                .block();
        return packs;
    }

    public WebClient packsWebClient(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        AuthResponse responseDto = (AuthResponse) session.getAttribute("connectedUserWithToken");
        return WebClient
                .builder()
                .filter(ErrorHandler.handle())
                .filter((request, next) -> next.exchange(
                        withBearerAuth(request, responseDto.getToken())
                ))
                .baseUrl(BackUrl.BACK_URL + "packs/")
                .build();
    }

    private static ClientRequest withBearerAuth(ClientRequest request, String token) {
        return ClientRequest.from(request)
                .header(com.google.common.net.HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .build();
    }


    public Booking saveBooking(BookingDto bookingDto){
        WebClient webClient = packsWebClient();
        return  webClient.post()
                .uri("addBooking")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .bodyValue(bookingDto)
                .retrieve()
                .bodyToMono(Booking.class)
                .block();
    }


    public List <Booking> getUserBookings(Integer userid){
        WebClient webClient = packsWebClient();
        List<Booking> bookings = webClient.get()
                .uri("user/" + userid + "/bookings")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Booking>>() {})
                .block();
        if (bookings != null) {
        }
        return bookings;
    }

    public List<Accommodation> findAllAccommodations() {
        List<Accommodation> accommodations = webClient.get()
                .uri("findAllAccommodations")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Accommodation>>() {})
                .block();
        return accommodations;
    }


    public void accommodationBooking(BookingDto bookingDto){
        WebClient webClient = packsWebClient();
        webClient.post()
                .uri("accommodationBooking")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .bodyValue(bookingDto)
                .retrieve()
                .bodyToMono(Booking.class)
                .block();
    }

    public List <Booking> userAccommodationsBookings(Integer id){
        WebClient webClient = packsWebClient();
        List<Booking> bookings = webClient.get()
                .uri("user/" + id + "/bookings")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Booking>>() {})
                .block();
        if (bookings != null) {
        }
        return bookings;
    }

    public List<Booking> findAllBookings() {
        List<Booking> bookings = webClient.get()
                .uri("findAllBookings")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Booking>>() {})
                .block();
        return bookings;
    }

    public void cancelBooking(Integer id) {
        WebClient webClient= packsWebClient();
        webClient.delete()
                .uri("cancelBooking"+ "/" + id)
                .exchange()
                .subscribe();
    }



}
