package fr.eql.ai113.lmtravelsFront.controller.webclient;

import fr.eql.ai113.lmtravelsFront.entity.Accommodation;
import fr.eql.ai113.lmtravelsFront.entity.Pack;
import fr.eql.ai113.lmtravelsFront.entity.dto.AccommodationDto;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthResponse;
import fr.eql.ai113.lmtravelsFront.entity.dto.PackDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.WebClient;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class NewPacksWebClient {


    public WebClient newPacksWebClient(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        AuthResponse responseDto = (AuthResponse) session.getAttribute("connectedUserWithToken");
        return WebClient
                .builder()
                .filter(ErrorHandler.handle())
                .filter((request, next) -> next.exchange(
                        withBearerAuth(request, responseDto.getToken())
                ))
                .baseUrl(BackUrl.BACK_URL + "newPacks/")
                .build();
    }

    private static ClientRequest withBearerAuth(ClientRequest request, String token) {
        return ClientRequest.from(request)
                .header(com.google.common.net.HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .build();
    }

    public Pack createPack(PackDto packDto) {
        WebClient webClient= newPacksWebClient();
        return webClient.post()
                .uri("createpack")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .bodyValue(packDto)
                .retrieve()
                .bodyToMono(Pack.class)
                .block();
    }

    public List<Pack> findAllPacks() {
        WebClient webClient= newPacksWebClient();
        List<Pack> packs = webClient.get()
                .uri("findAllPacks")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<Pack>>() {})
                .block();
        return packs;
    }


    public Pack updatePack(PackDto packDto) {
        WebClient webClient= newPacksWebClient();
        return webClient.post()
                .uri("update")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .bodyValue(packDto)
                .retrieve()
                .bodyToMono(Pack.class)
                .block();
    }

    public void deletePack(Integer id) {
        WebClient webClient= newPacksWebClient();
        webClient.delete()
                .uri("deletePack" + "/" + id)
                .exchange()
                .subscribe();
    }

    public Accommodation createAccommodation(AccommodationDto accommodationDto) {
        WebClient webClient= newPacksWebClient();
        return webClient.post()
                .uri("createAccommodation")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .bodyValue(accommodationDto)
                .retrieve()
                .bodyToMono(Accommodation.class)
                .block();
    }

    public Pack updateAccommodation(AccommodationDto accommodationDto) {
        WebClient webClient= newPacksWebClient();
        return webClient.post()
                .uri("updateAccommodation")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .bodyValue(accommodationDto)
                .retrieve()
                .bodyToMono(Pack.class)
                .block();
    }

    public void deleteAccommodation(Integer id) {
        WebClient webClient= newPacksWebClient();
        webClient.delete()
                .uri("deleteAccommodation" + "/" + id)
                .exchange()
                .subscribe();
    }


}
