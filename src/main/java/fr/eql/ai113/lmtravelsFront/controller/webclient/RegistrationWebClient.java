package fr.eql.ai113.lmtravelsFront.controller.webclient;

import fr.eql.ai113.lmtravelsFront.entity.Booking;
import fr.eql.ai113.lmtravelsFront.entity.User;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthResponse;
import fr.eql.ai113.lmtravelsFront.entity.dto.UserDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.WebClient;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class RegistrationWebClient {

    private final WebClient webClient;

    public RegistrationWebClient() {
        this.webClient = WebClient
                .builder()
                .filter(ErrorHandler.handle())
                .baseUrl(BackUrl.BACK_URL + "registration/")
                .build();
    }


    public User registration(UserDto userDto) {
        return webClient.post()
                .uri("createuser")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .bodyValue(userDto)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }

    public User registrationAdmin(UserDto userDto) {
        return webClient.post()
                .uri("createadmin")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .bodyValue(userDto)
                .retrieve()
                .bodyToMono(User.class)
                .block();
    }

    public List<User> findAllUsers() {
        List<User> users = webClient.get()
                .uri("findAllUsers")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<User>>() {})
                .block();
        return users;
    }

    public void deleteUser(Integer id) {
        webClient.delete()
                .uri("deleteUser" + "/" + id)
                .exchange()
                .subscribe();
    }



}
