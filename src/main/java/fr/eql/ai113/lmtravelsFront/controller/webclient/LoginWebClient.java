package fr.eql.ai113.lmtravelsFront.controller.webclient;

import fr.eql.ai113.lmtravelsFront.entity.User;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthRequest;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@Component
public class LoginWebClient {

    private final WebClient webClient;


    public LoginWebClient() {
        this.webClient = WebClient
                .builder()
                .filter(ErrorHandler.handle())
                .baseUrl(BackUrl.BACK_URL + "security/")
                .build();
    }

    public AuthResponse authenticate(AuthRequest requestDto) {
        AuthResponse connectedUserWithToken;
        try {
            connectedUserWithToken = webClient
                    .post()
                    .uri("authorize")
                    .bodyValue(requestDto)
                    .retrieve()
                    .bodyToMono(AuthResponse.class)
                    .block();
            if (connectedUserWithToken != null) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
                session.setAttribute("connectedUserWithToken", connectedUserWithToken);
            }
            return connectedUserWithToken;
        } catch (IllegalStateException e) {
            return null;
        }
    }
}
