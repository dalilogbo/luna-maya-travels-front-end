package fr.eql.ai113.lmtravelsFront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LmtravelsFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(LmtravelsFrontApplication.class, args);
	}

}
