package fr.eql.ai113.lmtravelsFront.util;

import javax.servlet.http.HttpSession;

import fr.eql.ai113.lmtravelsFront.entity.User;
import fr.eql.ai113.lmtravelsFront.entity.dto.AuthResponse;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class AuthenticationUtils {

    public static User getConnectedUserWithToken() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = attr.getRequest().getSession(false);
        if (session != null) {
            AuthResponse responseDto = (AuthResponse) session.getAttribute("connectedUserWithToken");
            if (responseDto != null) {
                return responseDto.getUser();
            }
        }
        return null;
    }

}
