package fr.eql.ai113.lmtravelsFront;

import org.apache.catalina.servlets.DefaultServlet;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.server.MimeMappings;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(LmtravelsFrontApplication.class);
	}

}
