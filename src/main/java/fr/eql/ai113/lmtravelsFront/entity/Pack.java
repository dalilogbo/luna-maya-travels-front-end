package fr.eql.ai113.lmtravelsFront.entity;

import java.time.LocalDate;
import java.util.List;

public class Pack {

    public Integer id;
    public String pack_name;
    public String pack_description;
    public String pack_photo;
    public Boolean pack_disponibility;
    public LocalDate pack_depart_date;
    public LocalDate pack_return_date;
    public Integer pack_quantity;
    public Integer pack_price;
    public String pack_accomodation;
    public String pack_transport;
    public String pack_activity;
    private Blog blog;
    private List<Booking> bookings;


    public Pack() {
    }

    public Pack(Integer id, String pack_name, String pack_description, String pack_photo, Boolean pack_disponibility, LocalDate pack_depart_date, LocalDate pack_return_date, Integer pack_quantity, Integer pack_price, String pack_accomodation, String pack_transport, String pack_activity, Blog blog, List<Booking> bookings) {
        this.id = id;
        this.pack_name = pack_name;
        this.pack_description = pack_description;
        this.pack_photo = pack_photo;
        this.pack_disponibility = pack_disponibility;
        this.pack_depart_date = pack_depart_date;
        this.pack_return_date = pack_return_date;
        this.pack_quantity = pack_quantity;
        this.pack_price = pack_price;
        this.pack_accomodation = pack_accomodation;
        this.pack_transport = pack_transport;
        this.pack_activity = pack_activity;
        this.blog = blog;
        this.bookings = bookings;
    }

    public Pack(String pack_name, String pack_description, String pack_photo, Boolean pack_disponibility, LocalDate pack_depart_date, LocalDate pack_return_date, Integer pack_price, String pack_accomodation, String pack_transport, String pack_activity) {
        this.pack_name = pack_name;
        this.pack_description = pack_description;
        this.pack_photo = pack_photo;
        this.pack_disponibility = pack_disponibility;
        this.pack_depart_date = pack_depart_date;
        this.pack_return_date = pack_return_date;
        this.pack_price = pack_price;
        this.pack_accomodation = pack_accomodation;
        this.pack_transport = pack_transport;
        this.pack_activity = pack_activity;
    }

    public Pack(Integer id, String pack_name, String pack_description, String pack_photo, Boolean pack_disponibility, LocalDate pack_depart_date, LocalDate pack_return_date, Integer pack_quantity, Integer pack_price, String pack_accomodation, String pack_transport, String pack_activity) {
        this.id = id;
        this.pack_name = pack_name;
        this.pack_description = pack_description;
        this.pack_photo = pack_photo;
        this.pack_disponibility = pack_disponibility;
        this.pack_depart_date = pack_depart_date;
        this.pack_return_date = pack_return_date;
        this.pack_quantity = pack_quantity;
        this.pack_price = pack_price;
        this.pack_accomodation = pack_accomodation;
        this.pack_transport = pack_transport;
        this.pack_activity = pack_activity;
    }

    /// Getters ///
    public Integer getId() {
        return id;
    }
    public String getPack_name() {
        return pack_name;
    }
    public String getPack_description() {
        return pack_description;
    }
    public String getPack_photo() {
        return pack_photo;
    }
    public Boolean getPack_disponibility() {
        return pack_disponibility;
    }
    public LocalDate getPack_depart_date() {
        return pack_depart_date;
    }
    public LocalDate getPack_return_date() {
        return pack_return_date;
    }
    public Integer getPack_quantity() {
        return pack_quantity;
    }
    public Integer getPack_price() {
        return pack_price;
    }

    public String getPack_accomodation() {
        return pack_accomodation;
    }

    public String getPack_transport() {
        return pack_transport;
    }

    public String getPack_activity() {
        return pack_activity;
    }

    public Blog getBlog() {
        return blog;
    }
    public List<Booking> getBookings() {
        return bookings;
    }




}
