package fr.eql.ai113.lmtravelsFront.entity;

import java.util.Date;
import java.util.List;

public class Accommodation {

    public Integer id;
    public  String accom_type;
    public  String accom_name;
    public  String accom_country;
    public  String accom_city;
    public  String accom_description;
    public  String accom_room_type;
    public  String accom_photo;
    public Boolean accom_disponibility;
    public Date accom_arrival_date;
    public Date accom_depart_date;
    public Integer accom_quantity;
    public Integer accom_price;
    private List<Booking> bookings;

    public Accommodation() {

    }

    public Accommodation(Integer id, String accom_type, String accom_name, String accom_country, String accom_city, String accom_description, String accom_room_type, String accom_photo, Boolean accom_disponibility, Date accom_arrival_date, Date accom_depart_date, Integer accom_quantity, Integer accom_price, List<Booking> bookings) {
        this.id = id;
        this.accom_type = accom_type;
        this.accom_name = accom_name;
        this.accom_country = accom_country;
        this.accom_city = accom_city;
        this.accom_description = accom_description;
        this.accom_room_type = accom_room_type;
        this.accom_photo = accom_photo;
        this.accom_disponibility = accom_disponibility;
        this.accom_arrival_date = accom_arrival_date;
        this.accom_depart_date = accom_depart_date;
        this.accom_quantity = accom_quantity;
        this.accom_price = accom_price;
        this.bookings = bookings;
    }

    public Accommodation(String accom_type, String accom_name, String accom_country, String accom_city, String accom_description, String accom_room_type, String accom_photo, Boolean accom_disponibility, Integer accom_price) {
        this.accom_type = accom_type;
        this.accom_name = accom_name;
        this.accom_country = accom_country;
        this.accom_city = accom_city;
        this.accom_description = accom_description;
        this.accom_room_type = accom_room_type;
        this.accom_photo = accom_photo;
        this.accom_disponibility = accom_disponibility;
        this.accom_price = accom_price;
    }



    /// Getters ///
    public Integer getId() {
        return id;
    }
    public String getAccom_type() {
        return accom_type;
    }
    public String getAccom_name() {
        return accom_name;
    }
    public String getAccom_country() {
        return accom_country;
    }
    public String getAccom_city() {
        return accom_city;
    }
    public String getAccom_description() {
        return accom_description;
    }
    public String getAccom_room_type() {
        return accom_room_type;
    }
    public String getAccom_photo() {
        return accom_photo;
    }
    public Boolean getAccom_disponibility() {
        return accom_disponibility;
    }
    public Date getAccom_arrival_date() {
        return accom_arrival_date;
    }
    public Date getAccom_depart_date() {
        return accom_depart_date;
    }
    public Integer getAccom_quantity() {
        return accom_quantity;
    }
    public Integer getAccom_price() {
        return accom_price;
    }
    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBooked(boolean b) {

    }
}
