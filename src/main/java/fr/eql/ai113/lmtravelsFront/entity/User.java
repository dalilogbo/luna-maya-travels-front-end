package fr.eql.ai113.lmtravelsFront.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class User {

    public Integer id;
    private final Collection<Role> roles = new ArrayList<>();
    public String name;
    public String surname;
    public String login;
    public String password;
    public String phone;
    public String address;
    private List<Comments> comments = new ArrayList<>();
    private List<Booking> bookings = new ArrayList<>();

    public User() {
    }

    public User(Integer id, String name, String surname, String login) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
    }

    /// Getters ///
    public Integer getId() {
        return id;
    }
    public Collection<Role> getRoles() {
        return roles;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
    public String getPhone() {
        return phone;
    }
    public String getAddress() {
        return address;
    }
    public List<Comments> getComments() {
        return comments;
    }
    public List<Booking> getBookings() {
        return bookings;
    }

    /// Setters ///
    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }
    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }


}
