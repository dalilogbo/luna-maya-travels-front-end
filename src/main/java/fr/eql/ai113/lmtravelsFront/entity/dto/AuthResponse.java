package fr.eql.ai113.lmtravelsFront.entity.dto;

import fr.eql.ai113.lmtravelsFront.entity.User;

public class AuthResponse {

    private User user;
    private String token;

    /// Getters ///
    public User getUser() {
        return user;
    }
    public String getToken() {
        return token;
    }
}
