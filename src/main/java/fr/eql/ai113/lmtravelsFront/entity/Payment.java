package fr.eql.ai113.lmtravelsFront.entity;

public class Payment {

    public Integer id;
    public Integer amount;
    public String mode;
    public String payment_date;
    private Booking booking;

    public Payment() {
    }

    public Payment(Integer id, Integer amount, String mode, String payment_date, Booking booking) {
        this.id = id;
        this.amount = amount;
        this.mode = mode;
        this.payment_date = payment_date;
        this.booking = booking;
    }

    /// Getters ///

    public Integer getId() {
        return id;
    }

    public Integer getAmount() {
        return amount;
    }

    public String getMode() {
        return mode;
    }

    public String getPayment_date() {
        return payment_date;
    }

    public Booking getBooking() {
        return booking;
    }
}
