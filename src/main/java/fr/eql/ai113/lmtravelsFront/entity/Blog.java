package fr.eql.ai113.lmtravelsFront.entity;

import java.util.List;

public class Blog {

    public Integer id;
    public String article_name;
    public String article_description;
    public String article_country;
    public String article_city;
    public String article_photo;
    private List<Pack> packs;

    public Blog() {
    }

    public Blog(Integer id, String article_name, String article_description, String article_country, String article_city, String article_photo, List<Pack> packs) {
        this.id = id;
        this.article_name = article_name;
        this.article_description = article_description;
        this.article_country = article_country;
        this.article_city = article_city;
        this.article_photo = article_photo;
        this.packs = packs;
    }

    /// Getters ///

    public Integer getId() {
        return id;
    }

    public String getArticle_name() {
        return article_name;
    }

    public String getArticle_description() {
        return article_description;
    }

    public String getArticle_country() {
        return article_country;
    }

    public String getArticle_city() {
        return article_city;
    }

    public String getArticle_photo() {
        return article_photo;
    }

    public List<Pack> getPacks() {
        return packs;
    }
}
