package fr.eql.ai113.lmtravelsFront.entity;

import java.sql.Date;
import java.util.List;

public class Activity {

    public Integer id;
    public String activity_type;
    public String activity_name;
    public String activity_country;
    public String activity_city;
    public  String activity_description;
    public  String activity_photo;
    public Boolean activity_disponibility;
    public Date activity_date;
    public Date activity_time;
    public Integer activity_quantity;
    public Integer activity_price;
    private List<Booking> bookings;

    public Activity() {
    }

    public Activity(Integer id, String activity_type, String activity_name, String activity_country, String activity_city, String activity_description, String activity_photo, Boolean activity_disponibility, Date activity_date, Date activity_time, Integer activity_quantity, Integer activity_price, List<Booking> bookings) {
        this.id = id;
        this.activity_type = activity_type;
        this.activity_name = activity_name;
        this.activity_country = activity_country;
        this.activity_city = activity_city;
        this.activity_description = activity_description;
        this.activity_photo = activity_photo;
        this.activity_disponibility = activity_disponibility;
        this.activity_date = activity_date;
        this.activity_time = activity_time;
        this.activity_quantity = activity_quantity;
        this.activity_price = activity_price;
        this.bookings = bookings;
    }

    /// Getters ///

    public Integer getId() {
        return id;
    }

    public String getActivity_type() {
        return activity_type;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public String getActivity_country() {
        return activity_country;
    }

    public String getActivity_city() {
        return activity_city;
    }

    public String getActivity_description() {
        return activity_description;
    }

    public String getActivity_photo() {
        return activity_photo;
    }

    public Boolean getActivity_disponibility() {
        return activity_disponibility;
    }

    public Date getActivity_date() {
        return activity_date;
    }

    public Date getActivity_time() {
        return activity_time;
    }

    public Integer getActivity_quantity() {
        return activity_quantity;
    }

    public Integer getActivity_price() {
        return activity_price;
    }

    public List<Booking> getBookings() {
        return bookings;
    }
}
