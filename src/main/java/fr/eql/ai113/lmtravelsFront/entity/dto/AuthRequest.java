package fr.eql.ai113.lmtravelsFront.entity.dto;

public class AuthRequest {

    private final String username;
    private final String password;

    public AuthRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /// Getters ///
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
}
