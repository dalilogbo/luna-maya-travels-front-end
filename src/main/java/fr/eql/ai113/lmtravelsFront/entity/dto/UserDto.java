package fr.eql.ai113.lmtravelsFront.entity.dto;

import fr.eql.ai113.lmtravelsFront.entity.Role;

import java.util.Collection;

public class UserDto {

    private Collection<Role> roles ;
    public Integer id;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String phone;
    private String address;

    public UserDto(Integer id, String name, String surname, String login, String password, String phone, String address) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    public UserDto(Collection<Role> roles, String name, String surname, String login, String password, String phone, String address) {
        this.roles = roles;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    public UserDto(String name, String surname, String login, String password, String phone, String address) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.address = address;
    }

    /// Getters ///

    public Collection<Role> getRoles() {
        return roles;
    }

    public Integer getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
    public String getPhone() {
        return phone;
    }
    public String getAddress() {
        return address;
    }

}
