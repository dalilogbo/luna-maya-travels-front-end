package fr.eql.ai113.lmtravelsFront.entity;

import java.util.ArrayList;
import java.util.List;

public class Role {

    private Integer id;
    private String name;
    private List<User> personsByRole = new ArrayList<>();

    ///Getters//
    public Integer getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public List<User> getPersonsByRole() {
        return personsByRole;
    }
}
