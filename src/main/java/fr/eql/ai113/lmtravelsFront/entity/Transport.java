package fr.eql.ai113.lmtravelsFront.entity;

import java.sql.Date;
import java.util.List;

public class Transport {

    public Integer id;
    public String transport_type;
    public String transport_name;
    public String from_country;
    public String from_city;
    public String to_country;
    public String to_city;
    public Boolean transp_disponibility;
    public Date transp_dept_date;
    public Date transp_dept_time;
    public Date transp_return_date;
    public Date transp_return_time;
    public Integer transport_quantity;
    public Integer transport_price;
    private List<Booking> bookings;

    public Transport() {
    }

    public Transport(Integer id, String transport_type, String transport_name, String from_country, String from_city, String to_country, String to_city, Boolean transp_disponibility, Date transp_dept_date, Date transp_dept_time, Date transp_return_date, Date transp_return_time, Integer transport_quantity, Integer transport_price, List<Booking> bookings) {
        this.id = id;
        this.transport_type = transport_type;
        this.transport_name = transport_name;
        this.from_country = from_country;
        this.from_city = from_city;
        this.to_country = to_country;
        this.to_city = to_city;
        this.transp_disponibility = transp_disponibility;
        this.transp_dept_date = transp_dept_date;
        this.transp_dept_time = transp_dept_time;
        this.transp_return_date = transp_return_date;
        this.transp_return_time = transp_return_time;
        this.transport_quantity = transport_quantity;
        this.transport_price = transport_price;
        this.bookings = bookings;
    }

    /// Getters ///

    public Integer getId() {
        return id;
    }

    public String getTransport_type() {
        return transport_type;
    }

    public String getTransport_name() {
        return transport_name;
    }

    public String getFrom_country() {
        return from_country;
    }

    public String getFrom_city() {
        return from_city;
    }

    public String getTo_country() {
        return to_country;
    }

    public String getTo_city() {
        return to_city;
    }

    public Boolean getTransp_disponibility() {
        return transp_disponibility;
    }

    public Date getTransp_dept_date() {
        return transp_dept_date;
    }

    public Date getTransp_dept_time() {
        return transp_dept_time;
    }

    public Date getTransp_return_date() {
        return transp_return_date;
    }

    public Date getTransp_return_time() {
        return transp_return_time;
    }

    public Integer getTransport_quantity() {
        return transport_quantity;
    }

    public Integer getTransport_price() {
        return transport_price;
    }

    public List<Booking> getBookings() {
        return bookings;
    }
}
