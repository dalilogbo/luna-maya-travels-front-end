package fr.eql.ai113.lmtravelsFront.entity;

import java.sql.Date;

public class Comments {

    public Integer id;
    public String user_message;
    public Date comment_date;
    private User user;

    public Comments() {
    }

    public Comments(Integer id, String user_message, Date comment_date, User user) {
        this.id = id;
        this.user_message = user_message;
        this.comment_date = comment_date;
        this.user = user;
    }

    /// Getters ///
    public Integer getId() {
        return id;
    }
    public String getUser_message() {
        return user_message;
    }
    public Date getComment_date() {
        return comment_date;
    }
    public User getUser() {
        return user;
    }
}
