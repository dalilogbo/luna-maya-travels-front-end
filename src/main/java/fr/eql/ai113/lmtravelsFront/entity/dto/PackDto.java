package fr.eql.ai113.lmtravelsFront.entity.dto;

import java.time.LocalDate;

public class PackDto {

    private Integer id;
    private String pack_name;
    private String pack_description;
    private String pack_photo;
    private Boolean pack_disponibility;
    private LocalDate pack_depart_date;
    private LocalDate pack_return_date;
    private Integer pack_price;
    public String pack_accomodation;
    public String pack_transport;
    public String pack_activity;

    public PackDto(String pack_name, String pack_description, String pack_photo, Boolean pack_disponibility, LocalDate pack_depart_date, LocalDate pack_return_date, Integer pack_price, String pack_accomodation, String pack_transport, String pack_activity) {
        this.pack_name = pack_name;
        this.pack_description = pack_description;
        this.pack_photo = pack_photo;
        this.pack_disponibility = pack_disponibility;
        this.pack_depart_date = pack_depart_date;
        this.pack_return_date = pack_return_date;
        this.pack_price = pack_price;
        this.pack_accomodation = pack_accomodation;
        this.pack_transport = pack_transport;
        this.pack_activity = pack_activity;
    }

    public PackDto(Integer id, String pack_name, String pack_description, String pack_photo, Boolean pack_disponibility, LocalDate pack_depart_date, LocalDate pack_return_date, Integer pack_price, String pack_accomodation, String pack_transport, String pack_activity) {
        this.id = id;
        this.pack_name = pack_name;
        this.pack_description = pack_description;
        this.pack_photo = pack_photo;
        this.pack_disponibility = pack_disponibility;
        this.pack_depart_date = pack_depart_date;
        this.pack_return_date = pack_return_date;
        this.pack_price = pack_price;
        this.pack_accomodation = pack_accomodation;
        this.pack_transport = pack_transport;
        this.pack_activity = pack_activity;
    }


    ///Getters///

    public Integer getId() {
        return id;
    }

    public String getPack_name() {
        return pack_name;
    }
    public String getPack_description() {
        return pack_description;
    }
    public String getPack_photo() {
        return pack_photo;
    }
    public Boolean getPack_disponibility() {
        return pack_disponibility;
    }
    public LocalDate getPack_depart_date() {
        return pack_depart_date;
    }
    public LocalDate getPack_return_date() {
        return pack_return_date;
    }
    public Integer getPack_price() {
        return pack_price;
    }
    public String getPack_accomodation() {
        return pack_accomodation;
    }
    public String getPack_transport() {
        return pack_transport;
    }
    public String getPack_activity() {
        return pack_activity;
    }
}
